(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/common/core/types/models/Plan.ts":
/*!**********************************************!*\
  !*** ./src/common/core/types/models/Plan.ts ***!
  \**********************************************/
/*! exports provided: Plan */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Plan", function() { return Plan; });
class Plan {
    constructor(params = {}) {
        this.currency_symbol = '$';
        this.interval = 'month';
        this.interval_count = 1;
        this.parent_id = null;
        this.recommended = false;
        this.show_permissions = false;
        this.free = false;
        this.position = 0;
        this.features = [];
        for (const name in params) {
            this[name] = params[name];
        }
    }
}
Plan.ctorParameters = () => [
    { type: Object }
];


/***/ }),

/***/ "./src/common/shared/billing/full-plan-name/full-plan-name.component.ts":
/*!******************************************************************************!*\
  !*** ./src/common/shared/billing/full-plan-name/full-plan-name.component.ts ***!
  \******************************************************************************/
/*! exports provided: FullPlanNameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullPlanNameComponent", function() { return FullPlanNameComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _common_core_types_models_Plan__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/core/types/models/Plan */ "./src/common/core/types/models/Plan.ts");
/* harmony import */ var _common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/core/translations/translations.service */ "./src/common/core/translations/translations.service.ts");
/* harmony import */ var _common_core_utils_uc_first__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/core/utils/uc-first */ "./src/common/core/utils/uc-first.ts");





let FullPlanNameComponent = class FullPlanNameComponent {
    constructor(i18n) {
        this.i18n = i18n;
    }
    getFullPlanName() {
        if (!this.plan)
            return;
        let name = this.plan.parent ? this.plan.parent.name : this.plan.name;
        name = Object(_common_core_utils_uc_first__WEBPACK_IMPORTED_MODULE_4__["ucFirst"])(this.i18n.t(name));
        name += ' ' + this.i18n.t('Plan');
        if (this.plan.parent)
            name += ': ' + this.plan.name;
        return name;
    }
};
FullPlanNameComponent.ctorParameters = () => [
    { type: _common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__["Translations"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _common_core_types_models_Plan__WEBPACK_IMPORTED_MODULE_2__["Plan"])
], FullPlanNameComponent.prototype, "plan", void 0);
FullPlanNameComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'full-plan-name',
        template: '{{getFullPlanName()}}',
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__["Translations"]])
], FullPlanNameComponent);



/***/ }),

/***/ "./src/common/shared/billing/full-plan-name/full-plan-name.module.ts":
/*!***************************************************************************!*\
  !*** ./src/common/shared/billing/full-plan-name/full-plan-name.module.ts ***!
  \***************************************************************************/
/*! exports provided: FullPlanNameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullPlanNameModule", function() { return FullPlanNameModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./full-plan-name.component */ "./src/common/shared/billing/full-plan-name/full-plan-name.component.ts");



let FullPlanNameModule = class FullPlanNameModule {
};
FullPlanNameModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__["FullPlanNameComponent"],
        ],
        exports: [
            _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__["FullPlanNameComponent"],
        ]
    })
], FullPlanNameModule);



/***/ }),

/***/ "./src/common/shared/billing/guards/billing-enabled-guard.service.ts":
/*!***************************************************************************!*\
  !*** ./src/common/shared/billing/guards/billing-enabled-guard.service.ts ***!
  \***************************************************************************/
/*! exports provided: BillingEnabledGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingEnabledGuard", function() { return BillingEnabledGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/config/settings.service */ "./src/common/core/config/settings.service.ts");




let BillingEnabledGuard = class BillingEnabledGuard {
    constructor(settings, router) {
        this.settings = settings;
        this.router = router;
    }
    canActivate(route, state) {
        return this.handle();
    }
    canActivateChild(route, state) {
        return this.handle();
    }
    handle() {
        if (this.settings.get('billing.integrated') && this.settings.get('billing.enable')) {
            return true;
        }
        this.router.navigate(['/']);
        return false;
    }
};
BillingEnabledGuard.ctorParameters = () => [
    { type: _core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__["Settings"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
BillingEnabledGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__["Settings"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], BillingEnabledGuard);



/***/ }),

/***/ "./src/common/shared/billing/plans.service.ts":
/*!****************************************************!*\
  !*** ./src/common/shared/billing/plans.service.ts ***!
  \****************************************************/
/*! exports provided: PLANS_BASE_URI, Plans */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLANS_BASE_URI", function() { return PLANS_BASE_URI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Plans", function() { return Plans; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/http/app-http-client.service */ "./src/common/core/http/app-http-client.service.ts");



const PLANS_BASE_URI = 'billing-plan';
let Plans = class Plans {
    constructor(http) {
        this.http = http;
    }
    all(params) {
        return this.http.get(PLANS_BASE_URI, params);
    }
    get(id) {
        return this.http.get(`${PLANS_BASE_URI}/${id}`);
    }
    create(params) {
        return this.http.post(PLANS_BASE_URI, params);
    }
    update(id, params) {
        return this.http.put(`${PLANS_BASE_URI}/${id}`, params);
    }
    delete(params) {
        return this.http.delete(`${PLANS_BASE_URI}/${params.ids}`);
    }
    sync() {
        return this.http.post(`${PLANS_BASE_URI}/sync`);
    }
};
Plans.ctorParameters = () => [
    { type: _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"] }
];
Plans = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"]])
], Plans);



/***/ }),

/***/ "./src/common/shared/billing/subscriptions.service.ts":
/*!************************************************************!*\
  !*** ./src/common/shared/billing/subscriptions.service.ts ***!
  \************************************************************/
/*! exports provided: Subscriptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subscriptions", function() { return Subscriptions; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/http/app-http-client.service */ "./src/common/core/http/app-http-client.service.ts");



let Subscriptions = class Subscriptions {
    /**
     * Subscriptions constructor.
     */
    constructor(http) {
        this.http = http;
    }
    /**
     * Get all available subscriptions.
     */
    all(params) {
        return this.http.get('billing/subscriptions', params);
    }
    /**
     * Get subscription matching specified id.
     */
    get(id) {
        return this.http.get('billing/subscriptions/' + id);
    }
    /**
     * Create a new subscription on stripe.
     */
    createOnStripe(params) {
        return this.http.post('billing/subscriptions/stripe', params);
    }
    /**
     * Update subscription matching specified id.
     */
    update(id, params) {
        return this.http.put('billing/subscriptions/' + id, params);
    }
    /**
     * Create a new subscription.
     */
    create(params) {
        return this.http.post('billing/subscriptions', params);
    }
    /**
     * Cancel subscription matching specified id.
     */
    cancel(id, params) {
        return this.http.delete('billing/subscriptions/' + id, params);
    }
    resume(id) {
        return this.http.post('billing/subscriptions/' + id + '/resume');
    }
    changePlan(id, plan) {
        return this.http.post('billing/subscriptions/' + id + '/change-plan', { newPlanId: plan.id });
    }
    addCard(token) {
        return this.http.post('billing/stripe/cards/add', { token });
    }
};
Subscriptions.ctorParameters = () => [
    { type: _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"] }
];
Subscriptions = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"]])
], Subscriptions);



/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map