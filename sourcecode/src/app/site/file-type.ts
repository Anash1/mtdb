export enum FILE_TYPE {
	
	JSON = 'JSON',
	XML  = 'XML',
	HTML = 'HTML'
}
