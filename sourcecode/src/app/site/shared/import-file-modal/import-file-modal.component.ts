import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FILE_TYPE} from '../../file-type' ; 
import {TitlesService} from '../../titles/titles.service';
import {finalize} from 'rxjs/operators';
import {Title} from '../../../models/title';
import {Person} from '../../../models/person';
import {Toast} from '@common/core/ui/toast.service';



@Component({
    selector: 'import-file-modal',
    templateUrl: './import-file-modal.component.html',
    styleUrls: ['./import-file-modal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportFileModalComponent {
    public loading$ = new BehaviorSubject(false);
    public importForm = this.fb.group({
        files: [],
        fileType: [],
    });

    constructor(
        private fb: FormBuilder,
        private toast: Toast, 
        private titles: TitlesService,
        private dialogRef: MatDialogRef<ImportFileModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.hydrateForm();
    }

    public confirm() {

        this.loading$.next(true);
        this.titles.importFromFile(this.importForm.getRawValue())
            .pipe(finalize(() => this.loading$.next(false)))
            .subscribe(response => {
                this.close();
            }, errorResponse => {
                // 403 error will already show error toast by default
                if (errorResponse.status !== 403) {
                    this.toast.open('There was an issue with importing this media item.');
                }
            });
    }

    public onSelectFile(event) {

        if (event.target.files && event.target.files[0]) {
                this.importForm.get('files').setValue(event.target.files) ;           
         }     
    }


    public close(mediaItem?: Title|Person) {
        this.dialogRef.close(mediaItem);
    }

    private hydrateForm() {
       
       this.data.fileTypes = [FILE_TYPE.JSON , FILE_TYPE.XML ] ; 

    }
}
