(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/common/core/types/models/Plan.ts":
/*!**********************************************!*\
  !*** ./src/common/core/types/models/Plan.ts ***!
  \**********************************************/
/*! exports provided: Plan */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Plan", function() { return Plan; });
var Plan = /** @class */ (function () {
    function Plan(params) {
        if (params === void 0) { params = {}; }
        this.currency_symbol = '$';
        this.interval = 'month';
        this.interval_count = 1;
        this.parent_id = null;
        this.recommended = false;
        this.show_permissions = false;
        this.free = false;
        this.position = 0;
        this.features = [];
        for (var name_1 in params) {
            this[name_1] = params[name_1];
        }
    }
    Plan.ctorParameters = function () { return [
        { type: Object }
    ]; };
    return Plan;
}());



/***/ }),

/***/ "./src/common/shared/billing/full-plan-name/full-plan-name.component.ts":
/*!******************************************************************************!*\
  !*** ./src/common/shared/billing/full-plan-name/full-plan-name.component.ts ***!
  \******************************************************************************/
/*! exports provided: FullPlanNameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullPlanNameComponent", function() { return FullPlanNameComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_core_types_models_Plan__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/core/types/models/Plan */ "./src/common/core/types/models/Plan.ts");
/* harmony import */ var _common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/core/translations/translations.service */ "./src/common/core/translations/translations.service.ts");
/* harmony import */ var _common_core_utils_uc_first__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/core/utils/uc-first */ "./src/common/core/utils/uc-first.ts");





var FullPlanNameComponent = /** @class */ (function () {
    function FullPlanNameComponent(i18n) {
        this.i18n = i18n;
    }
    FullPlanNameComponent.prototype.getFullPlanName = function () {
        if (!this.plan)
            return;
        var name = this.plan.parent ? this.plan.parent.name : this.plan.name;
        name = Object(_common_core_utils_uc_first__WEBPACK_IMPORTED_MODULE_4__["ucFirst"])(this.i18n.t(name));
        name += ' ' + this.i18n.t('Plan');
        if (this.plan.parent)
            name += ': ' + this.plan.name;
        return name;
    };
    FullPlanNameComponent.ctorParameters = function () { return [
        { type: _common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__["Translations"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _common_core_types_models_Plan__WEBPACK_IMPORTED_MODULE_2__["Plan"])
    ], FullPlanNameComponent.prototype, "plan", void 0);
    FullPlanNameComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'full-plan-name',
            template: '{{getFullPlanName()}}',
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_common_core_translations_translations_service__WEBPACK_IMPORTED_MODULE_3__["Translations"]])
    ], FullPlanNameComponent);
    return FullPlanNameComponent;
}());



/***/ }),

/***/ "./src/common/shared/billing/full-plan-name/full-plan-name.module.ts":
/*!***************************************************************************!*\
  !*** ./src/common/shared/billing/full-plan-name/full-plan-name.module.ts ***!
  \***************************************************************************/
/*! exports provided: FullPlanNameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullPlanNameModule", function() { return FullPlanNameModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./full-plan-name.component */ "./src/common/shared/billing/full-plan-name/full-plan-name.component.ts");



var FullPlanNameModule = /** @class */ (function () {
    function FullPlanNameModule() {
    }
    FullPlanNameModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__["FullPlanNameComponent"],
            ],
            exports: [
                _full_plan_name_component__WEBPACK_IMPORTED_MODULE_2__["FullPlanNameComponent"],
            ]
        })
    ], FullPlanNameModule);
    return FullPlanNameModule;
}());



/***/ }),

/***/ "./src/common/shared/billing/guards/billing-enabled-guard.service.ts":
/*!***************************************************************************!*\
  !*** ./src/common/shared/billing/guards/billing-enabled-guard.service.ts ***!
  \***************************************************************************/
/*! exports provided: BillingEnabledGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingEnabledGuard", function() { return BillingEnabledGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/config/settings.service */ "./src/common/core/config/settings.service.ts");




var BillingEnabledGuard = /** @class */ (function () {
    function BillingEnabledGuard(settings, router) {
        this.settings = settings;
        this.router = router;
    }
    BillingEnabledGuard.prototype.canActivate = function (route, state) {
        return this.handle();
    };
    BillingEnabledGuard.prototype.canActivateChild = function (route, state) {
        return this.handle();
    };
    BillingEnabledGuard.prototype.handle = function () {
        if (this.settings.get('billing.integrated') && this.settings.get('billing.enable')) {
            return true;
        }
        this.router.navigate(['/']);
        return false;
    };
    BillingEnabledGuard.ctorParameters = function () { return [
        { type: _core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__["Settings"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    BillingEnabledGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_config_settings_service__WEBPACK_IMPORTED_MODULE_3__["Settings"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], BillingEnabledGuard);
    return BillingEnabledGuard;
}());



/***/ }),

/***/ "./src/common/shared/billing/plans.service.ts":
/*!****************************************************!*\
  !*** ./src/common/shared/billing/plans.service.ts ***!
  \****************************************************/
/*! exports provided: PLANS_BASE_URI, Plans */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLANS_BASE_URI", function() { return PLANS_BASE_URI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Plans", function() { return Plans; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/http/app-http-client.service */ "./src/common/core/http/app-http-client.service.ts");



var PLANS_BASE_URI = 'billing-plan';
var Plans = /** @class */ (function () {
    function Plans(http) {
        this.http = http;
    }
    Plans.prototype.all = function (params) {
        return this.http.get(PLANS_BASE_URI, params);
    };
    Plans.prototype.get = function (id) {
        return this.http.get(PLANS_BASE_URI + "/" + id);
    };
    Plans.prototype.create = function (params) {
        return this.http.post(PLANS_BASE_URI, params);
    };
    Plans.prototype.update = function (id, params) {
        return this.http.put(PLANS_BASE_URI + "/" + id, params);
    };
    Plans.prototype.delete = function (params) {
        return this.http.delete(PLANS_BASE_URI + "/" + params.ids);
    };
    Plans.prototype.sync = function () {
        return this.http.post(PLANS_BASE_URI + "/sync");
    };
    Plans.ctorParameters = function () { return [
        { type: _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"] }
    ]; };
    Plans = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"]])
    ], Plans);
    return Plans;
}());



/***/ }),

/***/ "./src/common/shared/billing/subscriptions.service.ts":
/*!************************************************************!*\
  !*** ./src/common/shared/billing/subscriptions.service.ts ***!
  \************************************************************/
/*! exports provided: Subscriptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subscriptions", function() { return Subscriptions; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/http/app-http-client.service */ "./src/common/core/http/app-http-client.service.ts");



var Subscriptions = /** @class */ (function () {
    /**
     * Subscriptions constructor.
     */
    function Subscriptions(http) {
        this.http = http;
    }
    /**
     * Get all available subscriptions.
     */
    Subscriptions.prototype.all = function (params) {
        return this.http.get('billing/subscriptions', params);
    };
    /**
     * Get subscription matching specified id.
     */
    Subscriptions.prototype.get = function (id) {
        return this.http.get('billing/subscriptions/' + id);
    };
    /**
     * Create a new subscription on stripe.
     */
    Subscriptions.prototype.createOnStripe = function (params) {
        return this.http.post('billing/subscriptions/stripe', params);
    };
    /**
     * Update subscription matching specified id.
     */
    Subscriptions.prototype.update = function (id, params) {
        return this.http.put('billing/subscriptions/' + id, params);
    };
    /**
     * Create a new subscription.
     */
    Subscriptions.prototype.create = function (params) {
        return this.http.post('billing/subscriptions', params);
    };
    /**
     * Cancel subscription matching specified id.
     */
    Subscriptions.prototype.cancel = function (id, params) {
        return this.http.delete('billing/subscriptions/' + id, params);
    };
    Subscriptions.prototype.resume = function (id) {
        return this.http.post('billing/subscriptions/' + id + '/resume');
    };
    Subscriptions.prototype.changePlan = function (id, plan) {
        return this.http.post('billing/subscriptions/' + id + '/change-plan', { newPlanId: plan.id });
    };
    Subscriptions.prototype.addCard = function (token) {
        return this.http.post('billing/stripe/cards/add', { token: token });
    };
    Subscriptions.ctorParameters = function () { return [
        { type: _core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"] }
    ]; };
    Subscriptions = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_http_app_http_client_service__WEBPACK_IMPORTED_MODULE_2__["AppHttpClient"]])
    ], Subscriptions);
    return Subscriptions;
}());



/***/ })

}]);
//# sourceMappingURL=common-es5.js.map